# Project title
Project description 

# Understanding the folder structure
    .
    ├── config                  # Configuration files(alternatively `config`)
    ├── docs                    # Documentation files (alternatively `doc`)
    ├── models                  # Models (used for `inference`)
    ├── pytest                  # py tests (alternatively `tests`)
    ├── research                # Python Notebooks  
    ├── src                     # Python scripts 
    ├── Dockerfile              # Docker file 
    ├── LICENSE                 # LICENSE
    ├── main.py                 # what does the main script do?
    ├── requirements.txt        # dependencies of the project. 
    └── README.md

# How to reproduce the project in local environment?

This section must include steps that would help the viewer implement the project above in their own local machine. (this is where docker and virtual env comes in)


# Author
Your name
